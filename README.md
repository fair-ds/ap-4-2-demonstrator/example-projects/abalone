# FAIR-DS AP4.2 Demonstrator - Quality Assurance and Data Validation 

## What is this Repository?

This repository is an example project fo the AP4.2 Quality Assurance and Data Validation demonstrator. This repository uses a slightly modified version of the Abalone dataset from the UC Irvine Machine Learning repository ([link](https://archive.ics.uci.edu/dataset/1/abalone)).

The output from this demonstrator can be viewed at the following link: https://fair-ds.pages.rwth-aachen.de/ap-4-2-demonstrator/example-projects/abalone

The data here is run on the most recent version of the demonstrator anytime an update is performed. As a result, the output may occasionally contain broken links or missing items. 

## What is the objective of this Repository?

This Example project is intended to showcase a few specific aspects of the demonstrator. 

- Local Data stored in a GitLab Repository
  - As an alternative to S3 storage, we can optionally store datafiles directly in the repository. You can find the three data files used in the "data" directory. 
  - One of the data files - `abalone2.csv` - contains intentionally incorrect data to showcase how the demonstrator identifies and displays validation and quality issues.
- Handcrafted Schema files
  - We have provided a JSON file as a schema file in the "schemas" directory. This file is in the [Frictionless TableSchema](https://specs.frictionlessdata.io/table-schema/) format, and is applied to each of the datafiles. 
- TiB Ontology Service 
  - As a part of the FAIR-DS project, the Leibniz Information Centre for Science and Technology and University Library has created and published a public Ontology API Service ([link](https://service.tib.eu/ts4tib/ontologies)). 

## TiB Ontology Service
  
This demonstrator can retrieve data from this service in order to automatically complete schema data for fields which should apply to a particular ontology. In the `schemas/abalone_schema.json` file, at line 10 we see the following field definition:

```
...
{
    "name": "Sex",
    "type": "string",
    "ontology": "abcd::Sex",
    "constraints": {
        "required": true
    }
},
...
```
The field "ontology" contains a string, formatted as `{ontology name}::{ontology term}`. This information is used when the demonstrator runs to request information from the API which is used to complete the schema. In this instance, the field is expanded as follows:
```
...
{
    "name": "Sex",
    "type": "string",
    "ontology": "abcd::Sex",
    "title": "Sex",
    "description": "The sex of an organism.",
    "ontology_name": "ABCD Base Ontology",
    "date_issued": "2023-02-02 18:17:17.660000+00:00",
    "date_modified": "2023-09-19 17:06:23.152000+00:00",
    "constraints": {
        "required": True,
        "enum": [
            "Male",
            "Female",
            "SexUnknown",
            "SexNotApplicable",
            "MixedSex"
        ]
    }
},
...
```
The data here is sourced from the API endpoint that roughtly relates to [this page](https://service.tib.eu/ts4tib/ontologies/abcd/terms?iri=http%3A%2F%2Frs.tdwg.org%2Fabcd%2Fterms%2FSex&viewMode=All&siblings=false).
